﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HW3_2
{
    public partial class HobbiesPage : ContentPage
    {
        public HobbiesPage()
        {
            InitializeComponent();
        }
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left Hobbies Page", "Next");
        }
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You are entering the Hobbies Page", "Next");
        }
    }
}