﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HW3_2
{
    public partial class InterestsPage : ContentPage
    {
        public InterestsPage()
        {
            InitializeComponent();
        }
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left Interests Page", "Next");
        }
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You are entering the Interests Page", "Next");
        }
    }
}
