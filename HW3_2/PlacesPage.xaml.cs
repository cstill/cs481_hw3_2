﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HW3_2
{
    public partial class PlacesPage : ContentPage
    {
        public PlacesPage()
        {
            InitializeComponent();
        }
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left Places Page", "Next");
        }
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You are entering the Places Page", "Next");
        }
    }
}
